$(function () {
    "use strict";
    var placeholder = ' - your IP';

    var setMapHeight = function() {
        var table_height = $('.result-table').height();
        $('#map').height(table_height);
    }

    var setIPFlag = function(clear) {
        var myip = $('#my-ip').val();
        var input = $('.contact-form').find('input[name="ip"]');
        var ip = $.trim(input.val());
        if (ip.match(placeholder + '$')) {
            ip = ip.replace(placeholder, '');
        }
        if (!clear && ip === myip && ip !== '') {
            input.val(ip + placeholder);
        } else {
            input.val(ip);
        }

    }

    /* adjust map size when windows is resized */
    $(window).resize(setMapHeight);

    var send_request = function () {
        // Get ip address from element on the page:
        var $form = $('.contact-form'),
            ip = $.trim($form.find('input[name="ip"]').val()),
            url = $form.attr("action");
        if (ip.match(placeholder + '$')) {
            ip = ip.replace(placeholder, '');
        }
        // Send the data using post
        var isEmpty = function(str) {
            return (str.length === 0 || !str.trim());
        };

        if (!isEmpty(ip)) {

            var posting = $.post(url, {'ip': ip})

            // Put the results in a div
            posting.done(function (data) {
                if (data != null ) {

                    var anyKey = data.found == 1;
                    for(var prop in data) {
                        $('.'+prop).html(data[prop]);
                    }
                    if (anyKey) {
                        $('#not-found').css('display', 'none');
                        $('.result').css('display', 'block');
                        setMapHeight();

                        map.panTo([data.lat, data.lng]);
                        if (marker != null) {
                            map.removeLayer(marker);
                        }

                        var marker = L.marker([data.lat, data.lng]).addTo(map);
                    } else {
                        $('.result').css('display', 'none');
                        $('#not-found').css('display', 'block');
                        $('.ip').html(data.ip);
                    }

                }
            });
        }
    }

    $('.contact-form').on("submit", function (event) {

        // Stop form from submitting normally
        event.preventDefault();
        send_request();
    });

     $(document).ready(function () {

        setMapHeight();
        initMap();
        setIPFlag(false);
        var input = $('input[name="ip"]');
        input.focusin(function() {
            setIPFlag(true);
            var myip = $('#my-ip').val()
            if (input.val() === myip) {
                input.val('');
            }
        });

        input.focusout(function() {
            if (input.val() === '') {
                input.val($('#my-ip').val());
            }
            setIPFlag(false);
        })

    });
});
