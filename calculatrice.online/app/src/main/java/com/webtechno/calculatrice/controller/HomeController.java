package com.webtechno.calculatrice.controller;

import java.time.LocalDateTime;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
class HomeController {

    @GetMapping("/")
    String index(final Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "index";
    }

    @GetMapping("/base/calcul-puissance")
    String puissance(final Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "layout/puissance";
    }

    @GetMapping("/base/calcul-racine-carree")
    String racinecarree(final Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "layout/racine-carree";
    }

    @GetMapping("/base/calcul-carree-nombre")
    String careenombre(final Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "layout/carree-nombre";
    }

    @GetMapping("/base/calcul-logarithme")
    String logarithme(final Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "layout/logarithme";
    }

    @GetMapping("/base/nombre-premier")
    String nombrepremier(final Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "layout/nombre-premier";
    }

    @GetMapping("/base/fraction-irreductible")
    String fraction(final Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "layout/fraction-irreductible";
    }

    @GetMapping("properties")
    @ResponseBody
    java.util.Properties properties() {
        return System.getProperties();
    }

}
