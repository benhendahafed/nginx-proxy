package com.webtechno.iplocationfinder.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class IPLocationFinderFilter implements Filter {

    public static final String IP_ADDRESS = "userIpAdress";

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException,
            ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        final Map<String, String> result = new HashMap<>();

        final Enumeration headerNames = httpServletRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            final String key = (String) headerNames.nextElement();
            final String value = httpServletRequest.getHeader(key);
            result.put(key, value);
        }

        System.out.println(result);

        final String ipAddress = result.get("x-real-ip");
        request.setAttribute(IP_ADDRESS, ipAddress);
        chain.doFilter(request, response);
    }

    @Override
    public void init(final FilterConfig arg0) throws ServletException {

    }

}
