package com.webtechno.iplocationfinder.service;

import java.io.IOException;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Service
public class IPLocationFinderService {

    public static final String API_URL = "https://api.ipdata.co/";
    public static final String API_KEY_PREFIX = "?api-key=";

    @Value("${ip.location.finder.apiKey}")
    protected String API_KEY = "";

    public HashMap<String, Object> findIpLocation(final String lIPAdress) throws UnirestException, JsonParseException, JsonMappingException,
            IOException {
        final String url = API_URL + lIPAdress + API_KEY_PREFIX + API_KEY;
        System.out.println(url);
        final HttpResponse<String> response =
                Unirest.get(url).header("Cache-Control", "no-cache").header("Postman-Token", "9d13234d-786c-49aa-bcdb-c9d2f1c06940").asString();

        final String body = response.getBody();
        final HashMap<String, Object> result = new ObjectMapper().readValue(body, HashMap.class);
        return result;
    }
}
