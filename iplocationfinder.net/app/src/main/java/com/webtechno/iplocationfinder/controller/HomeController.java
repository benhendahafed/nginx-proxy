package com.webtechno.iplocationfinder.controller;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.thymeleaf.util.StringUtils;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.webtechno.iplocationfinder.filter.IPLocationFinderFilter;
import com.webtechno.iplocationfinder.service.IPLocationFinderService;

@Controller
class HomeController {

    public static String IP_VERSION = "ipversion";

    @Autowired
    protected IPLocationFinderService lIPLocationFinderService;

    @Value("${ipfinder.google.site.verification}")
    protected String lGoogleSiteVerification;

    @GetMapping("/")
    String index(final Model model) throws UnknownHostException {
        final RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        String lIPAdress = (String) attributes.getAttribute(IPLocationFinderFilter.IP_ADDRESS, RequestAttributes.SCOPE_REQUEST);
        HashMap<String, Object> response = null;
        if (StringUtils.isEmpty(lIPAdress))
            lIPAdress = "91.168.15.247";
        try {
            try {
                response = lIPLocationFinderService.findIpLocation(lIPAdress);
            } catch (final IOException e) {
                ;
            }
        } catch (final UnirestException e) {

        }

        final InetAddress lInetAddress = InetAddress.getByName(lIPAdress);
        if (lInetAddress instanceof Inet4Address) {
            model.addAttribute(IP_VERSION, "IPv4");
        } else if (lInetAddress instanceof Inet6Address) {
            model.addAttribute(IP_VERSION, "IPv6");
        }

        model.addAttribute("response", response);
        model.addAttribute("lGoogleSiteVerification", lGoogleSiteVerification);
        model.addAttribute("now", LocalDateTime.now());
        return "index";
    }

    @GetMapping("properties")
    @ResponseBody
    java.util.Properties properties() {
        return System.getProperties();
    }

    @GetMapping("/find")
    String index(final Model model, @RequestParam("ipAddress") final String ipAdress) {
        HashMap<String, Object> response = null;
        try {
            try {
                response = lIPLocationFinderService.findIpLocation(ipAdress);
            } catch (final IOException e) {
                ;
            }
        } catch (final UnirestException e) {

        }

        model.addAttribute("response", response);
        model.addAttribute("now", LocalDateTime.now());
        return "index";
    }
}
