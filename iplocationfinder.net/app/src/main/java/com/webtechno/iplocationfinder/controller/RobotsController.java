package com.webtechno.iplocationfinder.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RobotsController {

    @RequestMapping(value = {"/robots.txt", "/robot.txt" })
    @ResponseBody
    public String getRobotsTxt() {
        return "User-agent: *\n" + "Sitemap: https://iplocationfinder.net/static/sitemap.xml";
    }
}
